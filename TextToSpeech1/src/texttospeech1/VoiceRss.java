/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package texttospeech1;

import com.voicerss.tts.AudioCodec;
import com.voicerss.tts.AudioFormat;
import com.voicerss.tts.Languages;
import com.voicerss.tts.VoiceParameters;
import com.voicerss.tts.VoiceProvider;
import java.io.FileOutputStream;

/**
 *
 * @author linhtt4
 */
public class VoiceRss {

    public static void main(String[] args) throws Exception {
        VoiceProvider tts = new VoiceProvider("6e821fc2a3094a37bf0ab172926d78e7");

        VoiceParameters params = new VoiceParameters("It began with the idea of helping people lose weight and live healthier lives by providing them with the right diet tools and advice. The Cooking Light Diet stems from the American food and lifestyle magazine "
                + "“Cooking Light,” which has been around since 1987", Languages.English_UnitedStates);
        params.setCodec(AudioCodec.WAV);
        params.setFormat(AudioFormat.Format_44KHZ.AF_44khz_16bit_stereo);
        params.setBase64(false);
        params.setSSML(false);
        params.setRate(0);

        byte[] voice = tts.speech(params);

        FileOutputStream fos = new FileOutputStream("voice.mp3");
        fos.write(voice, 0, voice.length);
        fos.flush();
        fos.close();
    }
}
